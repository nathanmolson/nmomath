// Copyright (c) 2008-2016 Nathan Olson
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <math.h>

template <class type> Matrix3<type>::Matrix3()
{
  a11 = 0.0;
  a12 = 0.0;
  a13 = 0.0;
  a21 = 0.0;
  a22 = 0.0;
  a23 = 0.0;
  a31 = 0.0;
  a32 = 0.0;
  a33 = 0.0;
}

template <class type> Matrix3<type>::Matrix3(type x11, type x12, type x13, type x21, type x22, type x23, type x31, type x32, type x33)
{
  a11 = x11;
  a12 = x12;
  a13 = x13;
  a21 = x21;
  a22 = x22;
  a23 = x23;
  a31 = x31;
  a32 = x32;
  a33 = x33;
}

template <class type> Matrix3<type>::Matrix3(const Matrix3& A)
{
  a11 = A.a11;
  a12 = A.a12;
  a13 = A.a13;
  a21 = A.a21;
  a22 = A.a22;
  a23 = A.a23;
  a31 = A.a31;
  a32 = A.a32;
  a33 = A.a33;
}

template <class type> Matrix3<type>& Matrix3<type>::operator=(const Matrix3<type>& A)
{
  a11 = A.a11;
  a12 = A.a12;
  a13 = A.a13;
  a21 = A.a21;
  a22 = A.a22;
  a23 = A.a23;
  a31 = A.a31;
  a32 = A.a32;
  a33 = A.a33;
  return *this;
}

template <class type> Matrix3<type> operator*(const Matrix3<type>& A, const Matrix3<type>& B)
{
  Matrix3<type> C(A.a11*B.a11 + A.a12*B.a21 + A.a13*B.a31,
            A.a11*B.a12 + A.a12*B.a22 + A.a13*B.a32,
            A.a11*B.a13 + A.a12*B.a23 + A.a13*B.a33,
            A.a21*B.a11 + A.a22*B.a21 + A.a23*B.a31,
            A.a21*B.a12 + A.a22*B.a22 + A.a23*B.a32,
            A.a21*B.a13 + A.a22*B.a23 + A.a23*B.a33,
            A.a31*B.a11 + A.a32*B.a21 + A.a33*B.a31,
            A.a31*B.a12 + A.a32*B.a22 + A.a33*B.a32,
            A.a31*B.a13 + A.a32*B.a23 + A.a33*B.a33);
	return C;
}

template <class type> Vector3<type> operator*(const Matrix3<type>& A, const Vector3<type>& v)
{
  Vector3<type> w(A.a11*v.x + A.a12*v.y + A.a13*v.z,
            A.a21*v.x + A.a22*v.y + A.a23*v.z,
            A.a31*v.x + A.a32*v.y + A.a33*v.z);
  return w;
}

template <class type> Vector3<type> operator*(const Vector3<type>& v, const Matrix3<type>& A)
{
  Vector3<type> w(A.a11*v.x + A.a21*v.y + A.a31*v.z,
            A.a12*v.x + A.a22*v.y + A.a32*v.z,
            A.a13*v.x + A.a23*v.y + A.a33*v.z);
  return w;
}

template <class type> Matrix3<type> operator+(const Matrix3<type>& A, const Matrix3<type>& B)
{
	Matrix3<type> C(A.a11 + B.a11, A.a12 + B.a12, A.a13 + B.a13,
              A.a21 + B.a21, A.a22 + B.a22, A.a23 + B.a23,
              A.a31 + B.a31, A.a32 + B.a32, A.a33 + B.a33);
	return C;
}

template <class type> Matrix3<type> operator-(const Matrix3<type>& A, const Matrix3<type>& B)
{
	Matrix3<type> C(A.a11 - B.a11, A.a12 - B.a12, A.a13 - B.a13,
              A.a21 - B.a21, A.a22 - B.a22, A.a23 - B.a23,
              A.a31 - B.a31, A.a32 - B.a32, A.a33 - B.a33);
	return C;
}

template <class type> Matrix3<type> operator-(const Matrix3<type>& A)
{
	Matrix3<type> B(-A.a11, -A.a12, -A.a13, -A.a21, -A.a22, -A.a23, -A.a31, -A.a32, -A.a33);
	return B;
}

template <class type> Matrix3<type> operator*(const Matrix3<type>& A, type c)
{
  Matrix3<type> B(c*A.a11, c*A.a12, c*A.a13, c*A.a21, c*A.a22, c*A.a23, c*A.a31, c*A.a32, c*A.a33);
  return B;
}

template <class type> Matrix3<type> operator*(type c, const Matrix3<type>& A)
{
  return A*c;
}

template <class type> Matrix3<type> operator/(const Matrix3<type>& A, type c)
{
  Matrix3<type> B(c/A.a11, c/A.a12, c/A.a13, c/A.a21, c/A.a22, c/A.a23, c/A.a31, c/A.a32, c/A.a33);
  return B;
}

template <class type> bool operator==(const Matrix3<type>& A, const Matrix3<type>& B)
{
  return (A.a11 == B.a11 && A.a12 == B.a12 && A.a13 == B.a13 &&
          A.a21 == B.a21 && A.a22 == B.a22 && A.a23 == B.a23 &&
          A.a31 == B.a31 && A.a32 == B.a32 && A.a33 == B.a33) ? true : false;
}

template <class type> Matrix3<type> outer_product(const Vector3<type>& A, const Vector3<type>& B)
{
  Matrix3<type> M(A.x*B.x, A.x*B.y, A.x*B.z,
            A.y*B.x, A.y*B.y, A.y*B.z,
            A.z*B.x, A.z*B.y, A.z*B.z);
  return M;
}

template <class type> Matrix3<type> Matrix3<type>::transpose() const
{
  Matrix3<type> A(a11, a21, a31, a12, a22, a32, a13, a23, a33);
  return A;
}

template <class type> Matrix3<type> Matrix3<type>::inv() const
{
  type a11a22 = a11*a22;
  type a12a23 = a12*a23;
  type a13a21 = a13*a21;
  type a11a23 = a11*a23;
  type a12a21 = a12*a21;
  type a13a22 = a13*a22;

  type det = a11a22*a33 + a12a23*a31 + a13a21*a32
             - a11a23*a32 - a12a21*a33 - a13a22*a31;
  Matrix3<type> A((a22*a33 - a23*a32)/det,
            (a13*a32 - a12*a33)/det,
            (a12a23  -  a13a22)/det,
            (a31*a23 - a21*a33)/det,
            (a11*a33 - a13*a31)/det,
            (a13a21  -  a11a23)/det,
            (a21*a32 - a22*a31)/det,
            (a31*a12 - a11*a32)/det,
            (a11a22  -  a12a21)/det);
  return A;
}

template <class type> ostream& operator<<(ostream& os, Matrix3<type> A)
{
  os<<"[[ "<<A.a11<<" "<<A.a12<<" "<<A.a13<<" ]\n"
    <<" [ "<<A.a21<<" "<<A.a22<<" "<<A.a23<<" ]\n"
    <<" [ "<<A.a31<<" "<<A.a32<<" "<<A.a33<<" ]]";
	return os;
}

template class Matrix3<float>;
template class Matrix3<double>;
