// Copyright (c) 2008-2016 Nathan Olson
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef _ROTATIONMATRIX_H
#define _ROTATIONMATRIX_H

#include "Matrix3.h"
#include "Quaternion.h"

template <class type> class RotationMatrix : public Matrix3<type>
{
	public:

        using Matrix3<type>::a11;
        using Matrix3<type>::a12;
        using Matrix3<type>::a13;
        using Matrix3<type>::a21;
        using Matrix3<type>::a22;
        using Matrix3<type>::a23;
        using Matrix3<type>::a31;
        using Matrix3<type>::a32;
        using Matrix3<type>::a33;

        RotationMatrix();
        RotationMatrix(const Matrix3<type>&);
		RotationMatrix(type, type, type);
		RotationMatrix(const Quaternion<type>&);

        type roll() const;
        type pitch() const;
        type yaw() const;
};

#include "RotationMatrix.tcc"

#endif
