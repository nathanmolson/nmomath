// Copyright (c) 2008-2016 Nathan Olson
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <math.h>

template <class type> Quaternion<type>::Quaternion()
{
  q0 = 1.0;
  q1 = 0.0;
  q2 = 0.0;
  q3 = 0.0;
}

template <class type> Quaternion<type>::Quaternion(type a, type b, type c, type d)
{
  q0 = a;
  q1 = b;
  q2 = c;
  q3 = d;
}

//See Stevens Aircraft Control and Simulation Section 1.4
template <class type> Quaternion<type>::Quaternion(type roll, type pitch, type yaw)
{
	type cphi_2 = cos(roll/2.0);
	type sphi_2 = sin(roll/2.0);
	type ctheta_2 = cos(pitch/2.0);
	type stheta_2 = sin(pitch/2.0);
	type cpsi_2 = cos(yaw/2.0);
	type spsi_2 = sin(yaw/2.0);

	q0 = cpsi_2*ctheta_2*cphi_2 + spsi_2*stheta_2*sphi_2;
	q1 = cpsi_2*ctheta_2*sphi_2 - spsi_2*stheta_2*cphi_2;
	q2 = cpsi_2*stheta_2*cphi_2 + spsi_2*ctheta_2*sphi_2;
	q3 = spsi_2*ctheta_2*sphi_2 - cpsi_2*stheta_2*sphi_2;
}

template <class type> Quaternion<type>& Quaternion<type>::operator=(const Quaternion<type>& q)
{
	q0 = q.q0;
	q1 = q.q1;
	q2 = q.q2;
	q3 = q.q3;
	return *this;
}

template <class type> Quaternion<type>& Quaternion<type>::normalize()
{
	type length = sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3);
	q0 /= length;
	q1 /= length;
	q2 /= length;
	q3 /= length;
	return *this;
}

template <class type> Quaternion<type>& Quaternion<type>::normalize_QAD()
{
	type length = 0.5*(1.0 + q0*q0 + q1*q1 + q2*q2 + q3*q3);
	q0 /= length;
	q1 /= length;
	q2 /= length;
	q3 /= length;
	return *this;
}

template <class type> type Quaternion<type>::norm()
{
	return sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3);
}

template <class type> Quaternion<type> operator+(const Quaternion<type>& A, const Quaternion<type>& B)
{
  Quaternion<type> C(A.q0 + B.q0, A.q1 + B.q1, A.q2 + B.q2, A.q3 + B.q3);
  return C;
}

template <class type> Quaternion<type> operator-(const Quaternion<type>& A, const Quaternion<type>& B)
{
  Quaternion<type> C(A.q0 - B.q0, A.q1 - B.q1, A.q2 - B.q2, A.q3 - B.q3);
  return C;
}

template <class type> Quaternion<type> operator*(const Quaternion<type>& A, type c)
{
  Quaternion<type> B(A.q0*c, A.q1*c, A.q2*c, A.q3*c);
  return B;
}

template <class type> Quaternion<type> operator*(type c, const Quaternion<type>& A)
{
  return A*c;
}

//See Stevens Aircraft Control and Simulation Section 1.4
template <class type> Quaternion<type> rate(const Quaternion<type>& Q, const Vector3<type>& w)
{
  Quaternion<type> C(Q.q1*w.x + Q.q2*w.y + Q.q3*w.z,
              -Q.q0*w.x - Q.q2*w.z + Q.q3*w.y,
              -Q.q0*w.y + Q.q1*w.z - Q.q3*w.x,
              -Q.q0*w.z - Q.q1*w.y + Q.q2*w.x);
  return -0.5*C;
}

template <class type> ostream& operator<<(ostream& os, Quaternion<type> Q)
{
  os<<"<q "<<Q.q0<<" "<<Q.q1<<" "<<Q.q2<<" "<<Q.q3<<" q>";
  return os;
}

template class Quaternion<float>;
template class Quaternion<double>;
