// Copyright (c) 2008-2016 Nathan Olson
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <math.h>

template <class type> Vector3<type>::Vector3()
{
  x = 0.0;
  y = 0.0;
  z = 0.0;
}

template <class type> Vector3<type>::Vector3(type a, type b, type c)
{
	x = a;
	y = b;
	z = c;
}

template <class type> Vector3<type>::Vector3(const Vector3& A)
{
	x = A.x;
	y = A.y;
	z = A.z;
}

template <class type> type Vector3<type>::length()
{
    return sqrt(x*x + y*y + z*z);
}

template <class type> void Vector3<type>::normalize()
{
	type norm = length();
	if(norm > 0)
	{
		x /= norm;
		y /= norm;
		z /= norm;
	}
}

template <class type> Vector3<type>& Vector3<type>::operator=(const Vector3<type>& A)
{
	x = A.x;
	y = A.y;
	z = A.z;
	return *this;
}

template <class type> Vector3<type> operator+(const Vector3<type>& A, const Vector3<type>& B)
{
	Vector3<type> C(A.x + B.x, A.y + B.y, A.z + B.z);
	return C;
}

template <class type> Vector3<type> operator-(const Vector3<type>& A, const Vector3<type>& B)
{
	Vector3<type> C(A.x - B.x, A.y - B.y, A.z - B.z);
	return C;
}

template <class type> Vector3<type> operator-(const Vector3<type>& A)
{
	Vector3<type> B(-A.x, -A.y, -A.z);
	return B;
}

template <class type> Vector3<type> operator*(const Vector3<type>& A, type c)
{
	Vector3<type> B(c*A.x, c*A.y, c*A.z);
	return B;
}

template <class type> Vector3<type> operator*(type c, const Vector3<type>& A)
{
  return A*c;
}

template <class type> Vector3<type> operator/(const Vector3<type>& A, type c)
{
	Vector3<type> B(A.x/c, A.y/c, A.z/c);
	return B;
}

template <class type> bool operator==(const Vector3<type>& A, const Vector3<type>& B)
{
	return (A.x==B.x && A.y==B.y && A.z==B.z) ? true : false;
}

template <class type> type dot(const Vector3<type>& A, const Vector3<type>& B)
{
	return A.x * B.x + A.y * B.y + A.z * B.z;
}

template <class type> Vector3<type> cross(const Vector3<type>& A, const Vector3<type>& B)
{
	Vector3<type> C(A.y*B.z - A.z*B.y, A.z*B.x - A.x*B.z, A.x*B.y - A.y*B.x);
	return C;
}

template <class type> type norm(const Vector3<type>& A)
{
	return sqrt(A.x*A.x + A.y*A.y + A.z*A.z);
}

template <class type> Vector3<type> unit(const Vector3<type>& A)
{
	Vector3<type> B = A / norm(A);
	return B;
}

template <class type> ostream& operator<<(ostream& os, Vector3<type> A)
{
	os<<"< "<<A.x<<" "<<A.y<<" "<<A.z<<" >";
	return os;
}

template class Vector3<float>;
template class Vector3<double>;
