// Copyright (c) 2008-2016 Nathan Olson
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef _VECTOR3_H
#define _VECTOR3_H

#include <iostream>

using namespace std;

template <class type> class Vector3
{
	public:
		type x,y,z;

		Vector3();
		Vector3(type, type, type);
		Vector3(const Vector3&);

		void normalize();
		Vector3& operator=(const Vector3&);
		type length();
};

template <class type> Vector3<type> operator+(const Vector3<type>&, const Vector3<type>&);
template <class type> Vector3<type> operator-(const Vector3<type>&, const Vector3<type>&);
template <class type> Vector3<type> operator-(const Vector3<type>&);
template <class type> Vector3<type> operator*(const Vector3<type>&, type);
template <class type> Vector3<type> operator*(double, const Vector3<type>&);
template <class type> Vector3<type> operator/(const Vector3<type>&, type);
template <class type> bool operator==(const Vector3<type>&, const Vector3<type>&);
template <class type> type dot(const Vector3<type>&, const Vector3<type>&);
template <class type> Vector3<type> cross(const Vector3<type>&, const Vector3<type>&);
template <class type> type norm(const Vector3<type>&);
template <class type> Vector3<type> unit(const Vector3<type>&);
template <class type> ostream& operator<<(ostream&, Vector3<type>);

#include "Vector3.tcc"

#endif
