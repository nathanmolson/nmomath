// Copyright (c) 2008-2016 Nathan Olson
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <math.h>
#include "MathConstants.h"

template <class type> RotationMatrix<type>::RotationMatrix()
{
  a11 = 1.0;
  a12 = 0.0;
  a13 = 0.0;
  a21 = 0.0;
  a22 = 1.0;
  a23 = 0.0;
  a31 = 0.0;
  a32 = 0.0;
  a33 = 1.0;
}

template <class type> RotationMatrix<type>::RotationMatrix(const Matrix3<type>& A)
{
  a11 = A.a11;
  a12 = A.a12;
  a13 = A.a13;
  a21 = A.a21;
  a22 = A.a22;
  a23 = A.a23;
  a31 = A.a31;
  a32 = A.a32;
  a33 = A.a33;
}

//See Stevens Aircraft Control and Simulation Section 1.4
template <class type> RotationMatrix<type>::RotationMatrix(const Quaternion<type>& Q)
{
  type q0q0 = Q.q0*Q.q0;
  type q0q1 = Q.q0*Q.q1;
  type q0q2 = Q.q0*Q.q2;
  type q0q3 = Q.q0*Q.q3;
  type q1q1 = Q.q1*Q.q1;
  type q1q2 = Q.q1*Q.q2;
  type q1q3 = Q.q1*Q.q3;
  type q2q2 = Q.q2*Q.q2;
  type q2q3 = Q.q2*Q.q3;
  type q3q3 = Q.q3*Q.q3;

  a11 = q0q0 + q1q1 - q2q2 - q3q3;
  a12 = 2.0*(q1q2 + q0q3);
  a13 = 2.0*(q1q3 - q0q2);
  a21 = 2.0*(q1q2 - q0q3);
  a22 = q0q0 - q1q1 + q2q2 - q3q3;
  a23 = 2.0*(q2q3 + q0q1);
  a31 = 2.0*(q1q3 + q0q2);
  a32 = 2.0*(q2q3 - q0q1);
  a33 = q0q0 - q1q1 - q2q2 + q3q3;
}

//See Stevens Aircraft Control and Simulation Section 1.4
template <class type> RotationMatrix<type>::RotationMatrix(type roll, type pitch, type yaw)
{
  type cphi = cos(roll);
  type sphi = sin(roll);
  type ctheta = cos(pitch);
  type stheta = sin(pitch);
  type cpsi = cos(yaw);
  type spsi = sin(yaw);

  a11 = cpsi*ctheta;
  a12 = spsi*ctheta;
  a13 = -stheta;
  a21 = cpsi*stheta*sphi - spsi*cphi;
  a22 = spsi*stheta*sphi + cpsi*cphi;
  a23 = ctheta*sphi;
  a31 = cpsi*stheta*cphi + spsi*sphi;
  a32 = spsi*stheta*cphi - cpsi*sphi;
  a33 = ctheta*cphi;
}

template <class type> type RotationMatrix<type>::roll() const
{
  if(a23 == 0.0 && a33 == 0.0)
  {
    return 0.0;
  }
  else
  {
    return atan2(a23, a33);
  }
}

template <class type> type RotationMatrix<type>::pitch() const
{
   if(a13 < -1.0)
   {
     return PI_2;
   }
   else if(a13 > 1.0)
   {
     return -PI_2;
   }
   else
   {
     return -asin(a13);
   }
}

template <class type> type RotationMatrix<type>::yaw() const
{
  if(a11 == 0.0 && a12 == 0.0)
  {
    return 0.0; //TODO
  }
  else
  {
    return atan2(a12, a11);
  }
}

template <class type> RotationMatrix<type> operator*(const RotationMatrix<type>& A, const RotationMatrix<type>& B)
{
    return (RotationMatrix<type>) ((Matrix3<type>)A*(Matrix3<type>)B);
}
