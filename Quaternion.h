// Copyright (c) 2008-2016 Nathan Olson
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef _QUATERNION_H
#define _QUATERNION_H
#include <math.h>

#include "Vector3.h"

#include <iostream>

using namespace std;

template <class type> class Quaternion
{
	public:

		type q0, q1, q2, q3;

		Quaternion();
		Quaternion(type, type, type, type);
		Quaternion(type roll, type pitch, type yaw);

        Quaternion& operator=(const Quaternion&);
		Quaternion& normalize();
		Quaternion& normalize_QAD(); // "Quick And Dirty"
		type norm();
};

template <class type> Quaternion<type> operator+(const Quaternion<type>&, const Quaternion<type>&);
template <class type> Quaternion<type> operator-(const Quaternion<type>&, const Quaternion<type>&);
template <class type> Quaternion<type> operator*(const Quaternion<type>&, type);
template <class type> Quaternion<type> operator*(type, const Quaternion<type>&);
template <class type> Quaternion<type> rate(const Quaternion<type>&, const Vector3<type>&);
template <class type> ostream& operator<<(ostream&, Quaternion<type>);

#include "Quaternion.tcc"

#endif

